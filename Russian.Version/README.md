**Exam Text to SQL Converter (Russian Version)**

**Summary**\
This Python script does the exact same thing as the original version, but was redesigned for the Russian radio license exams.

**Why**\
After creating HAMtrainer I was contacted about making a new app for the Russian radio license exams, so I set about the first step of converting the exam to database instructions once again.
The exam texts weren't quite the same so the script had to be made differently. This version is a bit quicker and dirtier than the previous one but it works well and packs everything into fewer lines.
To fit the schema I created a separate exam for each qualification level, even though they use the same questions. The switch feature works in the same way so the script can be used on each exams to make blocks of SQL that can be easily combined.

The text I used is included in the `Exams` folder for reference and demonstration. It should be relatively simple to modify your test to the same style.
These are sourced from the original exam found here: https://grfc.ru/upload/medialibrary/92a/vopros_radiolyubitelyam.pdf

To use the script simply run it in the same directory as your exam text. The script will prompt you for a test character and the exam text file name. 
If your exam conforms to the specifications laid out above, the script will process the text into SQL commands and write them to a new .txt file with 
"sql" appended to the front of the original file name. The script will present you with counts of each element which should help you in determining 
offsets and if the exam is formatted correctly.

Note: This script is set up to be used with Python3. There are no new features involved so it can be run with Python2 but you will need to change the "input" commands.

**License**\
Because this is used to create a new version of what was laid out in funktrainer, I have chosen to also make this script available under the Apache V2 license.
