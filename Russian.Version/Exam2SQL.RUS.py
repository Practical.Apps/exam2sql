#Set file name and open
valid=False
while valid == False:
    #File Name entry
    tf=input("File Name: ")
    #Check entry
    if tf[len(tf)-4:] != ".txt":
        print("Not a .txt file")
    else:
        try:
            f = open(tf)
            print("File accepted")
            valid=True
        except IOError:
            print("Not a valid file name / file not in this directory")


#Get test type
valid=False
while valid == False:
    #Test flag entry
    tcraw=input("Test number: ")
    try:
        if int(tcraw) > 0 and int(tcraw) < 5:
            tflag = int(tcraw)
            valid = True
        else:
            print("Not a valid test number (1/2/3/4)")
            continue
    except ValueError:
        print("Not a valid test number (1/2/3/4)")
        continue

# Name, Offset: C, Q, A
testswitch = {
        1: ["высшей",5,0,0],
        2: ["базовой",13,426,1704],
        3: ["новичка",21,831,3324],
        4: ["начальной",29,1049,4196]
    }

#Set test type
oplist=testswitch.get(int(tcraw))
print(oplist[0] + " exam selected, index number " + tcraw + ". Offsets: C:" + str(oplist[1]) + " Q:" + str(oplist[2]) + " A:" + str(oplist[3]))

clist = []
qlist = ['x']
qnum = '0'
qflag = False
alist = []
apos = ['a)', 'b)', 'c)', 'd)']
count = 0
imgc = 0
imgl = []

#List building
with open(tf) as f: 
    for line in f:
        if line.startswith("группа"):
            lsp = line.split(":")
            # Number, group description (-1 for \n)
            clist.append((lsp[0][-1],lsp[1][1:-1]))
        elif line.startswith("Вопрос"):
            lsp = line.split(" ")
            qnum = lsp[1][2:-1]
            qflag = True
        elif qflag:
            # Number, question (strip for \n and trailing space)
            qlist.append([qnum, line.strip(), clist[-1][0]])
            qflag = False
            count += 1
        elif line.startswith("[IMAGE]"):
            imgl.append(str(count))
            qlist[count][1] = qlist[count][1] + "<p><img src=''" + str(count) + ".png''>"
            imgc += 1
        elif line[:2] in apos:
            alist.append((qnum,line[0],line[2:].strip()))

f.close()

if len(alist) == 4*(len(qlist)-1):
    print("Question and Answer counts match")

tsql = []
csql = []
qsql = []
q2t = []
q2c = []
c2t = []
asql = []
fastcat = []

anslist = ['d', 'd', 'c', 'b', 'b', 'd', 'a', 'a', 'd', 'b', 'a', 'a', 'a', 'b', 'a', 'a', 'c', 'd', 'a', 'b',
           'c', 'b', 'c', 'b', 'c', 'c', 'a', 'd', 'd', 'c', 'b', 'b', 'a', 'c', 'a', 'c', 'c', 'd', 'c', 'c',
           'd', 'b', 'd', 'c', 'c', 'a', 'a', 'd', 'b', 'c', 'a', 'c', 'b', 'b', 'c', 'c', 'd', 'a', 'c', 'd',
           'b', 'd', 'b', 'a', 'b', 'd', 'b', 'c', 'c', 'a', 'c', 'd', 'c', 'd', 'b', 'a', 'a', 'd', 'c', 'd',
           'b', 'c', 'd', 'd', 'b', 'b', 'd', 'a', 'c', 'd', 'c', 'd', 'd', 'b', 'a', 'b', 'c', 'a', 'a', 'a',
           'a', 'c', 'd', 'd', 'b', 'd', 'c', 'a', 'b', 'b', 'c', 'a', 'b', 'a', 'c', 'a', 'a', 'd', 'a', 'd',
           'a', 'a', 'b', 'c', 'd', 'b', 'b', 'a', 'c', 'b', 'c', 'd', 'b', 'a', 'a', 'b', 'a', 'c', 'd', 'd',
           'c', 'd', 'c', 'd', 'b', 'd', 'a', 'b', 'b', 'd', 'c', 'c', 'c', 'd', 'd', 'b', 'c', 'd', 'c', 'd',
           'a', 'a', 'b', 'd', 'c', 'b', 'b', 'a', 'a', 'b', 'd', 'd', 'a', 'a', 'b', 'a', 'd', 'd', 'c', 'c',
           'b', 'c', 'd', 'a', 'd', 'b', 'b', 'd', 'd', 'c', 'a', 'd', 'a', 'a', 'c', 'd', 'c', 'c', 'a', 'a',
           'c', 'd', 'a', 'a', 'b', 'b', 'a', 'c', 'b', 'd', 'a', 'c', 'c', 'd', 'b', 'b', 'd', 'c', 'c', 'b',
           'b', 'b', 'b', 'd', 'c', 'c', 'b', 'a', 'c', 'a', 'b', 'a', 'b', 'd', 'd', 'b', 'd', 'c', 'a', 'c',
           'a', 'c', 'c', 'a', 'a', 'a', 'd', 'b', 'd', 'd', 'd', 'c', 'a', 'b', 'c', 'd', 'a', 'a', 'a', 'c',
           'd', 'd', 'b', 'd', 'a', 'a', 'd', 'c', 'a', 'b', 'a', 'a', 'c', 'a', 'c', 'a', 'a', 'b', 'a', 'c',
           'c', 'c', 'c', 'b', 'b', 'c', 'd', 'd', 'a', 'a', 'c', 'd', 'b', 'd', 'd', 'b', 'c', 'b', 'c', 'd',
           'a', 'c', 'a', 'c', 'b', 'd', 'c', 'c', 'b', 'a', 'd', 'c', 'c', 'd', 'd', 'c', 'b', 'd', 'b', 'c',
           'd', 'b', 'a', 'a', 'c', 'a', 'b', 'b', 'b', 'c', 'a', 'b', 'd', 'c', 'd', 'c', 'd', 'b', 'd', 'c',
           'b', 'c', 'd', 'b', 'c', 'd', 'b', 'd', 'b', 'd', 'c', 'd', 'a', 'b', 'a', 'b', 'b', 'c', 'c', 'b',
           'c', 'a', 'd', 'd', 'd', 'b', 'b', 'b', 'a', 'b', 'c', 'c', 'b', 'a', 'd', 'c', 'a', 'b', 'c', 'c',
           'd', 'd', 'd', 'a', 'd', 'a', 'd', 'a', 'd', 'b', 'a', 'b', 'b', 'd', 'b', 'c', 'a', 'a', 'a', 'd',
           'b', 'c', 'c', 'c', 'b', 'c', 'a', 'c', 'c', 'a', 'd', 'b', 'd', 'c', 'c', 'c', 'c', 'a', 'b', 'b',
           'd', 'b', 'd', 'c', 'b', 'c']

ansdict = {'a': 0, 'b': 1, 'c': 2, 'd': 3}

#SQL command list builder

#topic - (id, order_index, name, isprimary);
tsql.append("INSERT INTO topic VALUES (" + tcraw + ", " + tcraw + ", '" + oplist[0] + " квалификация (" + tcraw + ")', 1);")

#category - (id, name, reference, isprimary, parent);
#category_to_topic - (id, category_id INT!=0 REFERENCES category(_id), topic_id INT!=0 REFERENCES topic(_id));
csql.append("INSERT INTO category VALUES (0, 'темами база', 'X', 1, 0);")
csql.append("INSERT INTO category VALUES (" + tcraw + ", '" + oplist[0] + " квалификация', '" + tcraw + "', 1, 0);")
c2t.append("INSERT INTO category_to_topic VALUES (" + tcraw + ", " + tcraw + ", " + tcraw + ");")

for c in range(0,len(clist)):
    csql.append("INSERT INTO category VALUES (" + str(c + oplist[1]) + ", '" + clist[c][1] + "', 'к" + tcraw + "т" + clist[c][0] + "', 0, " + tcraw + ");")
    c2t.append("INSERT INTO category_to_topic VALUES (" + str(c + oplist[1]) + ", " + str(c + oplist[1]) + ", " + tcraw + ");")

#question - (id, reference TXT, question TXT!='', level INT!=Null, next_time INT!=Null, wrong INT, correct INT, help TXT);
#question_to_topic - (id, question_id INT!=0 REFERENCES question(_id), topic_id INT!=0 REFERENCES topic(_id));
#question_to_category - (id, question_id INT!=0 REFERENCES question(_id), category_id INT!=0 REFERENCES category(_id));
for q in range(1,len(qlist)):
    qsql.append("INSERT INTO question VALUES (" + str(oplist[2]+q) + ", 'В" + qlist[q][0] + "', '" + qlist[q][1] + "', 0, 1, 0, 0, '');")
    q2t.append("INSERT INTO question_to_topic VALUES (" + str(oplist[2]+q) + ", " + str(oplist[2]+q) + ", " + tcraw + ");")
    q2c.append("INSERT INTO question_to_category VALUES (" + str(oplist[2]+q) + ", " + str(oplist[2]+q) + ", " + str(oplist[1]+int(qlist[q][2])-1) + ");")

ag = []
ac = 1
qc = 1
#answer - (id, question_id INT!=0 REFERENCES question(_id), order_index INT, answer TXT!='', help TEXT);
for a in range(0,len(alist)):
    if alist[a][1] != 'd':
        ag.append(alist[a])
    else:
        ag.append(alist[a])
        answer = anslist[int(alist[a][0])-1]  # Right answer
        asql.append("INSERT INTO answer VALUES (" + str(oplist[3]+ac) +  ", " + str(oplist[2]+qc) +  ", 0, '" + alist[a][2] + "', ' ');")
        ac += 1
        ag.remove(alist[a])
        for b in range(0,3):  # Wrong answers
            asql.append("INSERT INTO answer VALUES (" + str(oplist[3]+ac) +  ", " + str(oplist[2]+qc) +  ", " + str(b+1) + ", '" + ag[b][2] + "', ' ');")
            ac += 1
        ag = []
        qc += 1
    
newlist=[tsql,csql,qsql,asql,c2t,q2t,q2c]
print('SQL conversion completed, preparing to save...')

#Write to new file
newfile="sql"+tf
f=open(newfile,"w+")

for sublist in newlist:
    for element in sublist:
        newline = element + "\n"
        f.write(newline)
    f.write("\n")

f.close()

print('Successfully saved to file: "' + newfile + '"')
