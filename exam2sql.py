"""
Copyright 2020 William Westcott
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.
"""

#"Regular Expression" is the only dependency
import re

#Index number: Name, Offset: Answer,Category,Question
testswitch = {
        1: ["Technician",1,4,1],
        2: ["General",1693,49,424],
        3: ["Extra",3509,94,878],
    }

#Get test type
valid=False
while valid == False:
    #Test flag entry
    tcraw=input("Test character: ")
    tflag=tcraw.upper()
    if tflag == "T":
        tind = 1
        valid=True
    elif tflag == "G":
        tind = 2
        valid=True
    elif tflag == "E":
        tind = 3
        valid=True
    else:
        print("Not a valid test character (T/G/E)")

#Set test type
oplist=testswitch.get(tind)
print(oplist[0] + " exam selected, index number " + str(tind) + ". Offsets: C:" + str(oplist[2]) + " Q:" + str(oplist[3]) + " A:" + str(oplist[1]))

#Set file name and open
valid=False
while valid == False:
    #File Name entry
    tf=input("File Name: ")
    #Check entry
    if tf[len(tf)-4:] != ".txt":
        print("Not a .txt file")
    else:
        try:
            f = open(tf)
            print("File accepted")
            valid=True
        except IOError:
            print("Not a valid file name / file not in this directory")

#Read Exam File

count = 0
count2 = 0
selist = []
selc = 0
clist = []
clc = 0
qlist = []
qlc = 0
alist = []
alc = 0
check = ""
flag = 'O'

print("Processing file...")

#List building
with open(tf) as f: 
    for line in f:
        count += 1
        check = line.strip()
        if "'" in check:  # Single apostrophes break the .sql formatting
            check = check.replace("'","''")  # Replacing with double apostrophes fixes the problem

        #Extended sort (for continued lines)
        #Subelement
        if flag == 'S':
            if re.search('^'+tflag+'[0-9][A-Z] [-–] ',check) == None:
                selist[selc-1] += " " + check
            else:
                flag = 'O'
        #Category
        elif flag == 'C':
            if re.search('^'+tflag+'[0-9][A-Z][0-9][0-9]',check) == None:
                clist[clc-1] += " " + check
            else:
                flag = 'O'
        #Question
        elif flag == 'Q':
            if re.search('^A\. ',check) == None:
                qlist[qlc-1] += " "
                qlist[qlc-1] += check
            else:
                flag = 'O'
        elif flag == 'A':
            if re.search('^[ABCD]\. ',check) == None and check != "~~" and check != "":
                if re.search('^'+tflag+'[0-9][A-Z][0-9][0-9]',check) == None and re.search('^'+tflag+'[0-9][A-Z] [-–] ',check) == None:
                    if re.search('^SUBELEMENT',check) == None:
                        alist[-1] += " " + check
            else:
                flag = 'O'
        else:
            flag = 'O'

        #General Sort
        #Subelement
        if re.search('^SUBELEMENT',check):
            selist.append(check)
            #print('Subelement match found line: ' + str(count))
            selc += 1
            flag = 'S'
        #Category
        elif re.search('^'+tflag+'[0-9][A-Z] [-–] ',check):
            clist.append(check)
            #print('Category match found line: ' + str(count))
            clc += 1
            flag = 'C'
        #Question
        elif re.search('^'+tflag+'[0-9][A-Z][0-9][0-9]',check):
            qlist.append(check)
            qlc += 1
            flag = 'Q'
        #Answer
        elif re.search('^[ABCD]\. ',check):
            alist.append(check)
            alc += 1
            flag = 'A'
        else:
            count2 += 1

print("Results: ")
print("SELC:" + str(selc) + ", CLC:" + str(clc) + ", QLC:" + str(qlc) + ", ALC:" + str(alc))
if alc == 4*qlc:
    print("Question and Answer counts match")

f.close()

#Create SQL File

tsql = []
csql = []
qsql = []
asql = []
fastcat = []

#SQL command list builder

#topic (_id, order_index, name, isprimary);
tsql.append("INSERT INTO topic VALUES (" + str(tind) + ", " + str(tind) + ", '" + str(oplist[0]) + " License (" + str(tflag) + ")', 1);")

#Category
#(_id, name, reference, isprimary, parent);
csql.append("INSERT INTO category VALUES (0, 'Category base', 'X', 1, 0);")
csql.append("INSERT INTO category VALUES (" + str(tind) + ", '" + str(oplist[0]) + " License', '" + str(tflag) + "', 1, 0);")

i=0
i2=oplist[2]
i3=0
i4=0
subellen=0
seref=0

for subel in selist:
    semod = re.split('SUBELEMENT | – | - ',subel)
    csql.append("INSERT INTO category VALUES (" + str(i2) + ", '" + semod[2] + "', '"+ semod[1] + "', 0, " + str(tind) + ");")
    nt=re.match('[0-9]',semod[-1])
    subellen=int(nt.group(0))
    seref=i2
    i2+=1
    i3=i4
    i4+=subellen
    for i in range(i3,i4):
        catmod = re.split(' [-–] ',clist[i])
        csql.append("INSERT INTO category VALUES ("+ str(i2) + ", '" + catmod[1] + "', '" + catmod[0] + "', 0, " + str(seref) + ");")
        fastcat.append(catmod[0] + ',' + str(i2))
        i2+=1

#question
#(_id INT not null, reference TXT, question TXT!='', level INT not null, next_time INT not null, wrong INT, correct INT, help TXT);
i=oplist[3]
i2=0
i3=0
i4=oplist[1]
i5=0
for question in qlist:
    #Check if question already contains a help citation
    if re.search('^'+tflag+'[0-9][A-Z][0-9][0-9] \([A-D]\) \[',question) == None:
        qmod = re.split(' \(|\) ', question, maxsplit=2)
        qtext = qmod[2]
        htext = ''
    else:
        #qmod = re.split('\) \[| \(|\] |\) ', question, maxsplit=3)
        qmod = re.split('\)| \(', question, maxsplit=2)
        qmod2 = re.split('\] | \[', qmod[2], maxsplit=2)
        qmod[2]=qmod2[1]
        qmod.append(qmod2[2])
        qtext = qmod[3]
        htext = qmod[2]
    qsql.append("INSERT INTO question VALUES (" + str(i) + ", '" + qmod[0] + "', '" + qtext + "', 0, 1, 0, 0, '" + htext + "');")
    
    #answer
    #(_id INT not null, question_id INT not null REFERENCES question(_id), order_index INT, answer TXT!='', help TEXT);
    #add right answer
    answer=re.search('\([ABCD]\)',question).group(0)[1]
    ansnum=ord(answer)-65+i3
    amod = re.split('. ', alist[ansnum], maxsplit=1)
    asql.append("INSERT INTO answer VALUES (" + str(ansnum+i4) +  ", " + str(i) +  ", 0, '" + amod[1] + "', ' ');")
    i5 += 1
    #wrong answers
    for i2 in range(i3,i3+4):
        if i2 == ansnum:
            continue;
        else:
            amod = re.split('. ', alist[i2], maxsplit=1)
            asql.append("INSERT INTO answer VALUES (" + str(i2+i4) +  ", " + str(i) +  ", " + str(i5) + ", '" + amod[1] + "', ' ');")
            i5 += 1
    i+=1
    i3+=4
    if i5 == 4:
        i5 = 0

#Secondary Sort

#category_to_topic
#(_id INT not null, category_id INT not null REFERENCES category(_id), topic_id INT not null REFERENCES topic(_id));

c2tsql = []
c2ti=oplist[2]

c2tsql.append("INSERT INTO category_to_topic VALUES (" + str(tind) + ", " + str(tind) + ", " + str(tind) + ");")

for i in range(2,len(csql)):
    catmod = re.split('INSERT INTO category VALUES \(|, ',csql[i])
    c2tsql.append("INSERT INTO category_to_topic VALUES (" + str(c2ti) + ", " + catmod[1] + ", " + str(tind) + ");")
    c2ti+=1

#print(*c2tsql, sep='\n')

q2tsql = []
q2csql = []
q2ti=oplist[3]
q2ci=oplist[3]

#question_to_topic
#(_id INT not null, question_id INT not null REFERENCES question(_id), topic_id INT not null REFERENCES topic(_id));
for i in range(len(qsql)):
    qmod = re.split('INSERT INTO question VALUES \(|, ',qsql[i])
    q2tsql.append("INSERT INTO question_to_topic VALUES (" + str(q2ti) + ", " + qmod[1] + ", " + str(tind) + ");")
    q2ti+=1
    
    #question_to_category
    #(_id INT not null, question_id INT not null REFERENCES question(_id), category_id INT not null REFERENCES category(_id));
    qtag = re.search(tflag+'[0-9][A-M]',qmod[2]).group(0)
    for cat in fastcat:
        if cat.startswith(qtag):
            fcmod = re.split(',',cat)
            q2csql.append("INSERT INTO question_to_category VALUES (" + str(q2ci) + ", " + qmod[1] + ", " +  fcmod[1] + ");")
            break;
    q2ci+=1

newlist=[tsql,csql,qsql,asql,c2tsql,q2tsql,q2csql]
print('SQL conversion completed, preparing to save...')

#Write to new file
newfile="sql"+tf[:-3]+"sql"
f=open(newfile,"w+")

for sublist in newlist:
    for element in sublist:
        newline = element + "\n"
        f.write(newline)
    f.write("\n")

f.close()

print('Successfully saved to file: "' + newfile + '"')
