**Exam Text to SQL Converter**

**Summary**\
This Python script converts exam text of a certain format to SQL commands that fit the schema used in the [HAMtrainer](https://gitlab.com/Practical.Apps/ham.trainer) Android study application.

**Why**\
When creating HAMtrainer I had to come up with a way to convert the raw text of the American ham radio license exams to the SQL schema from the [funktrainer](https://github.com/meyerd/funktrainer) app. 
To do so manually would be time consuming and incredibly boring. Python is very easy to program with, especially when it comes to strings and manipulating text. 
These applications are very convenient for studying and any test which fits the same format can be converted to this SQL format and easily dropped into place. 
I originally made a separate script for each exam but now with the switch feature the same script can be used on multiple exams to make blocks of SQL that can be easily combined.

**How**\
More than anything, whatever test you use will have to conform to the same format and style as the radio license exams.\
Generally that is:
   - Multiple choice (4 Answer)
   - Exam > Subelement > Category > Question hierarchy
   - Number of categories in subelement header
   - "~~" separator between questions
   - .txt file extension

The text I used is included in the `Exam Texts` folder for reference and demonstration. It should be relatively simple to modify your test to the same style. 
If for whatever reason you cannot make your test fit this pattern you will have to adjust the script to fit your parameters. This script is set up for three specific 
exams that are combined together (See the [HAMtrainer main SQL file](https://gitlab.com/Practical.Apps/ham.trainer/-/blob/b37a416a9c0427d98c72294f31ea9005139d5aa0/app/src/main/assets/database_main_16.sql) 
to see the final product). If you intend to use the script for a different exam(s) you will have to modify the switch at the beginning of the script to for your particular situation. 
For example, a single test would have a single entry with no offsets (1,2,1).

To use the script simply run it in the same directory as your exam text. The script will prompt you for a test character and the exam text file name. 
If your exam conforms to the specifications laid out above, the script will process the text into SQL commands and write them to a new .txt file with 
"sql" appended to the front of the original file name. The script will present you with counts of each element which should help you in determining 
offsets and if the exam is formatted correctly.

Note: This script is set up to be used with Python3. There are no new features involved so it can be run with Python2 but you will need to change the "input" commands.

**License**\
Because this is used to create a new version of what was laid out in funktrainer, I have chosen to also make this script available under the Apache V2 license.